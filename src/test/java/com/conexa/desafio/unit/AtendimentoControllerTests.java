package com.conexa.desafio.unit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.conexa.desafio.controllers.AtendimentoController;
import com.conexa.desafio.data.MedicoDetails;
import com.conexa.desafio.models.Medico;
import com.conexa.desafio.models.Paciente;
import com.conexa.desafio.services.AtendimentoService;
import com.conexa.desafio.services.MedicoDetailsService;
import com.conexa.desafio.services.MedicoService;
import com.conexa.desafio.services.PacienteService;
import com.conexa.desafio.services.SessaoService;
import com.conexa.desafio.utils.JWTUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(AtendimentoController.class)
public class AtendimentoControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicoDetailsService medicoDetailsService;
    @MockBean
    private AtendimentoService atendimentoService;
    @MockBean
    private PacienteService pacienteService;
    @MockBean
    private MedicoService medicoService;
    @MockBean
    private SessaoService sessaoService;

    private String token;

    @Before
    public void setup() {
        Medico medico = new Medico();
        medico.setEmail("luis@email.com");
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        medico.setSenha(encoder.encode("12345"));

        MedicoDetails medicoDetails = new MedicoDetails(medico);

        String JWTToken = JWTUtil.createToken(medicoDetails);
        this.token = JWTToken;
    }

    @Test
    public void criarAtendimentoSemDataHoraDeveRetornar400() throws Exception {
        String atendimentoSemDataHora = "{\"cpfPaciente\":\"43404836057\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/attendance")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(atendimentoSemDataHora)
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarAtendimentoComDataHoraNoPassadoDeveRetornar400() throws Exception {
        String atendimentoComDataHoraNoPassado = "{\"dataHora\":\"2000-02-02T09:00\",\"cpfPaciente\":\"43404836057\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/attendance")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(atendimentoComDataHoraNoPassado)
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarAtendimentoComDataHoraNoFormatoErradoDeveRetornar400() throws Exception {
        String atendimentoComDataHoraNoFormatoErrado = "{\"dataHora\":\"2050-02-02 09:00\",\"cpfPaciente\":\"43404836057\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/attendance")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(atendimentoComDataHoraNoFormatoErrado)
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarAtendimentoSemCpfPacienteDeveRetornar400() throws Exception {
        String atendimentoSemCpfPaciente = "{\"dataHora\":\"2050-02-02T09:00\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/attendance")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(atendimentoSemCpfPaciente)
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarAtendimentoComPacienteNaoCadastradoDeveRetornar404() throws Exception {
        when(sessaoService.verificarSeTokenValido(any(String.class))).thenReturn(true);
        when(pacienteService.buscarPaciente(any(String.class))).thenReturn(null);
        when(medicoService.buscarMedicoPeloEmail(any(String.class))).thenReturn(new Medico());

        String atendimentoComPacienteNaoCadastrado = "{\"dataHora\":\"2050-02-02T09:00\",\"cpfPaciente\":\"43404836057\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/attendance")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(atendimentoComPacienteNaoCadastrado)
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void criarAtendimentoTokenInvalidoDeveRetornar401() throws Exception {
        when(sessaoService.verificarSeTokenValido(any(String.class))).thenReturn(false);
        when(pacienteService.buscarPaciente(any(String.class))).thenReturn(new Paciente());
        when(medicoService.buscarMedicoPeloEmail(any(String.class))).thenReturn(new Medico());

        String atendimentoTokenInvalido = "{\"dataHora\":\"2050-02-02T09:00\",\"cpfPaciente\":\"43404836057\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/attendance")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(atendimentoTokenInvalido)
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(401));
    }

    @Test
    public void criarAtendimentoSemEnviarTokenDeveRetornar403() throws Exception {
        when(sessaoService.verificarSeTokenValido(any(String.class))).thenReturn(true);
        when(pacienteService.buscarPaciente(any(String.class))).thenReturn(new Paciente());
        when(medicoService.buscarMedicoPeloEmail(any(String.class))).thenReturn(new Medico());

        String atendimentoSemEnviarToken = "{\"dataHora\":\"2050-02-02T09:00\",\"cpfPaciente\":\"43404836057\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/attendance")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(atendimentoSemEnviarToken))
            .andExpect(MockMvcResultMatchers.status().is(403));
    }

    @Test
    public void criarAtendimentoDeveRetornar201() throws Exception {
        when(sessaoService.verificarSeTokenValido(any(String.class))).thenReturn(true);
        when(pacienteService.buscarPaciente(any(String.class))).thenReturn(new Paciente());
        when(medicoService.buscarMedicoPeloEmail(any(String.class))).thenReturn(new Medico());

        String atendimento = "{\"dataHora\":\"2050-02-02T09:00\",\"cpfPaciente\":\"43404836057\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/attendance")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(atendimento)
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(201));
    }
}
