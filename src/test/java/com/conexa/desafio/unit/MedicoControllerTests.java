package com.conexa.desafio.unit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.conexa.desafio.controllers.MedicoController;
import com.conexa.desafio.models.Especialidade;
import com.conexa.desafio.models.Medico;
import com.conexa.desafio.services.EspecialidadeService;
import com.conexa.desafio.services.MedicoDetailsService;
import com.conexa.desafio.services.MedicoService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(MedicoController.class)
public class MedicoControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicoDetailsService medicoDetailsService;
    @MockBean
    private MedicoService medicoService;
    @MockBean
    private EspecialidadeService especialidadeService;

    @Test
    public void criarMedicoSemEmailDeveRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoSemEmail = "{\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoSemEmail))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComEmailInvalidoDeveRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoComEmailInvalido = "{\"email\":\"luis\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComEmailInvalido))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComEmailJaCadastradoDeveRetornar409() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);
        when(medicoService.buscarMedicoPeloEmail(any(String.class))).thenReturn(new Medico());

        String medicoComEmailJaCadastrado = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComEmailJaCadastrado))
            .andExpect(MockMvcResultMatchers.status().is(409));
    }

    @Test
    public void criarMedicoSemSenhaDeveRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoSemSenha = "{\"email\":\"luis\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoSemSenha))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComSenhaInvalidaDeveRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoComEmailInvalido = "{\"email\":\"luis@email.com\",\"senha\":\"1234\",\"confirmacaoSenha\":\"1234\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComEmailInvalido))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoSemConfirmacaoSenhaDeveRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoSemConfirmacaoSenha = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoSemConfirmacaoSenha))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComSenhasDiferentesDeveRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(false);

        String medicoComSenhasDiferentes = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComSenhasDiferentes))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoSemCpfDeveRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoSemCpf = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoSemCpf))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComCpfDiferenteDe11CaracteresRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoComCpfDiferentesDe11Caracteres = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"410702\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComCpfDiferentesDe11Caracteres))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComCpfComCaracterDiferenteDeNumeroRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoComCpfComCaracterDiferenteDeNumero = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"4107025306a\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComCpfComCaracterDiferenteDeNumero))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComCpfInvalidoRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoComCpfInvalido = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253088\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComCpfInvalido))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComCpfJaCadastradoDeveRetornar409() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);
        when(medicoService.buscarMedicoPeloCpf(any(String.class))).thenReturn(new Medico());

        String medicoComCpfJaCadastrado = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComCpfJaCadastrado))
            .andExpect(MockMvcResultMatchers.status().is(409));
    }

    @Test
    public void criarMedicoSemDataDeNascimentoRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoSemDataDeNascimento = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoSemDataDeNascimento))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComDataDeNascimentoNoFuturoRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoComDataDeNascimentoNoFuturo = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"2222-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComDataDeNascimentoNoFuturo))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoSemTelefoneRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoSemTelefone = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"2222-11-15\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoSemTelefone))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComTelefoneDiferenteDe10Ou11CaracteresRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoComTelefoneDiferenteDe10Ou11Caracteres = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"1299\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComTelefoneDiferenteDe10Ou11Caracteres))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComTelefoneComCaracterDiferenteDeNumeroRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoComTelefoneComCaracterDiferenteDeNumero = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"129999999a9\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComTelefoneComCaracterDiferenteDeNumero))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoSemEspecialidadeRetornar400() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);

        String medicoSemEspecialidade = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoSemEspecialidade))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarMedicoComEspecialidadeNaoCadastradaRetornar404() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);
        when(especialidadeService.buscarEpecialidade(any(String.class))).thenReturn(null);

        String medicoComEspecialidadeNaoCadastrada = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medicoComEspecialidadeNaoCadastrada))
            .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    public void criarMedicoDeveRetornar201() throws Exception {
        when(medicoService.verificarSeSenhasCoincidem(any(String.class), any(String.class))).thenReturn(true);
        when(especialidadeService.buscarEpecialidade(any(String.class))).thenReturn(new Especialidade());

        String medico = "{\"email\":\"luis@email.com\",\"senha\":\"12345\",\"confirmacaoSenha\":\"12345\",\"cpf\":\"41070253065\",\"dataNascimento\":\"1998-11-15\",\"telefone\":\"12999999999\",\"especialidade\":\"Cardiologia\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/signup")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(medico))
            .andExpect(MockMvcResultMatchers.status().is(201));
    }
}
