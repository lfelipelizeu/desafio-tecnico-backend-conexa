package com.conexa.desafio.unit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.conexa.desafio.controllers.EspecialidadeController;
import com.conexa.desafio.models.Especialidade;
import com.conexa.desafio.services.EspecialidadeService;
import com.conexa.desafio.services.MedicoDetailsService;
import com.conexa.desafio.utils.JsonUtil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(EspecialidadeController.class)
public class EspecialidadeControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicoDetailsService medicoDetailsService;
    @MockBean
    private EspecialidadeService especialidadeService;

    @Test
    public void criarEspecialidadeSemNomeDeveRetornar400() throws Exception {
        Especialidade especialidadeSemNome = new Especialidade();

        mockMvc.perform(MockMvcRequestBuilders.post("/especialidades")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(especialidadeSemNome)))
            .andExpect(MockMvcResultMatchers.status().is(400))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void criarEspecialidadeDeveRetornar201() throws Exception {
        Especialidade especialidade = new Especialidade();
        especialidade.setNome("Cardiologia");

        when(especialidadeService.criarEspecialidade(any(Especialidade.class))).thenReturn(especialidade);

        mockMvc.perform(MockMvcRequestBuilders.post("/especialidades")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(especialidade)))
            .andExpect(MockMvcResultMatchers.status().is(201))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void criarEspecialidadeRepetidaDeveRetornar409() throws Exception {
        when(especialidadeService.verificarSeJaExisteEspecialidade(any(String.class))).thenReturn(true);

        Especialidade especialidadeRepetida = new Especialidade();
        especialidadeRepetida.setNome("Cardiologia");

        mockMvc.perform(MockMvcRequestBuilders.post("/especialidades")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(especialidadeRepetida)))
            .andExpect(MockMvcResultMatchers.status().is(409))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void listarEspecialidadesDeveRetornar200() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/especialidades"))
            .andExpect(MockMvcResultMatchers.status().is(200));
    }
}
