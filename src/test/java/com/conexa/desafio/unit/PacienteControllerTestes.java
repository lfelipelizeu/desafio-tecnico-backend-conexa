package com.conexa.desafio.unit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.conexa.desafio.controllers.PacienteController;
import com.conexa.desafio.data.MedicoDetails;
import com.conexa.desafio.models.Medico;
import com.conexa.desafio.models.Paciente;
import com.conexa.desafio.services.MedicoDetailsService;
import com.conexa.desafio.services.PacienteService;
import com.conexa.desafio.services.SessaoService;
import com.conexa.desafio.utils.JWTUtil;
import com.conexa.desafio.utils.JsonUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(PacienteController.class)
public class PacienteControllerTestes {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicoDetailsService medicoDetailsService;
    @MockBean
    private PacienteService pacienteService;
    @MockBean
    private SessaoService sessaoService;

    private String token;

    @Before
    public void setup() {
        Medico medico = new Medico();
        medico.setEmail("luis@email.com");
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        medico.setSenha(encoder.encode("12345"));

        MedicoDetails medicoDetails = new MedicoDetails(medico);

        String JWTToken = JWTUtil.createToken(medicoDetails);
        this.token = JWTToken;
    }

    @Test
    public void criarPacienteSemNomeDeveRetornar400() throws Exception {
        Paciente pacienteSemNome = new Paciente();
        pacienteSemNome.setCpf("41477311041");

        mockMvc.perform(MockMvcRequestBuilders.post("/pacientes")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(pacienteSemNome))
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarPacienteSemCpfDeveRetornar400() throws Exception {
        Paciente pacienteSemCpf = new Paciente();
        pacienteSemCpf.setNome("Luis");

        mockMvc.perform(MockMvcRequestBuilders.post("/pacientes")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(pacienteSemCpf))
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarPacienteComCpfDiferenteDe11CaracteresDeveRetornar400() throws Exception {
        Paciente pacienteComCpfDiferenteDe11Caracteres = new Paciente();
        pacienteComCpfDiferenteDe11Caracteres.setNome("Luis");
        pacienteComCpfDiferenteDe11Caracteres.setCpf("4147");

        mockMvc.perform(MockMvcRequestBuilders.post("/pacientes")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(pacienteComCpfDiferenteDe11Caracteres))
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarPacienteComCpfDiferenteDeNumeroDeveRetornar400() throws Exception {
        Paciente pacienteComCpfDiferenteDeNumero = new Paciente();
        pacienteComCpfDiferenteDeNumero.setNome("Luis");
        pacienteComCpfDiferenteDeNumero.setCpf("4147731104a");

        mockMvc.perform(MockMvcRequestBuilders.post("/pacientes")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(pacienteComCpfDiferenteDeNumero))
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarPacienteComCpfInvalidoDeveRetornar400() throws Exception {
        when(sessaoService.verificarSeTokenValido(any(String.class))).thenReturn(true);

        Paciente pacienteComCpfInvalido = new Paciente();
        pacienteComCpfInvalido.setNome("Luis");
        pacienteComCpfInvalido.setCpf("41477311077");

        mockMvc.perform(MockMvcRequestBuilders.post("/pacientes")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(pacienteComCpfInvalido))
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    public void criarPacienteCpfComJaCadastradoDeveRetornar409() throws Exception {
        when(sessaoService.verificarSeTokenValido(any(String.class))).thenReturn(true);
        when(pacienteService.verificarSeJaExistePaciente(any(String.class))).thenReturn(true);

        Paciente pacienteCpfComJaCadastrado = new Paciente();
        pacienteCpfComJaCadastrado.setNome("Luis");
        pacienteCpfComJaCadastrado.setCpf("41477311041");

        mockMvc.perform(MockMvcRequestBuilders.post("/pacientes")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(pacienteCpfComJaCadastrado))
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(409));
    }

    @Test
    public void criarPacienteSemEnviarTokenDeveRetornar403() throws Exception {
        when(sessaoService.verificarSeTokenValido(any(String.class))).thenReturn(true);
        when(pacienteService.verificarSeJaExistePaciente(any(String.class))).thenReturn(false);
    
        Paciente paciente = new Paciente();
        paciente.setNome("Luis");
        paciente.setCpf("41477311041");

        mockMvc.perform(MockMvcRequestBuilders.post("/pacientes")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(paciente)))
            .andExpect(MockMvcResultMatchers.status().is(403));
    }

    @Test
    public void criarPacienteComTokenNaBlacklistDeveRetornar401() throws Exception {
        when(sessaoService.verificarSeTokenValido(any(String.class))).thenReturn(false);
        when(pacienteService.verificarSeJaExistePaciente(any(String.class))).thenReturn(false);
    
        Paciente paciente = new Paciente();
        paciente.setNome("Luis");
        paciente.setCpf("41477311041");

        mockMvc.perform(MockMvcRequestBuilders.post("/pacientes")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(paciente))
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(401));
    }

    @Test
    public void criarPacienteDeveRetornar201() throws Exception {
        when(sessaoService.verificarSeTokenValido(any(String.class))).thenReturn(true);
        when(pacienteService.verificarSeJaExistePaciente(any(String.class))).thenReturn(false);
    
        Paciente paciente = new Paciente();
        paciente.setNome("Luis");
        paciente.setCpf("41477311041");

        mockMvc.perform(MockMvcRequestBuilders.post("/pacientes")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(JsonUtil.toJson(paciente))
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    public void listarPacientesDeveRetornar200() throws Exception {
        when(sessaoService.verificarSeTokenValido(any(String.class))).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.get("/pacientes")
            .header("Authorization", "Bearer " + this.token))
            .andExpect(MockMvcResultMatchers.status().is(200));
    }
}
