package com.conexa.desafio.repositories;

import com.conexa.desafio.models.Medico;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicoRepository extends JpaRepository<Medico, Long> {
    public Medico findOneByCpf(String cpf);
    public Medico findOneByEmail(String email);
}
