package com.conexa.desafio.repositories;

import com.conexa.desafio.models.TokenBlacklist;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenBlacklistRepository extends JpaRepository<TokenBlacklist, Long> {
    public TokenBlacklist findOneByToken(String token);
}
