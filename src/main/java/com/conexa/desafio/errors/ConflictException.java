package com.conexa.desafio.errors;

public class ConflictException extends RuntimeException {
    public ConflictException(String message) {
		super(message);
	}
}
