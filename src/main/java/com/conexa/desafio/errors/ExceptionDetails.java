package com.conexa.desafio.errors;

public class ExceptionDetails {
    private String title;
	private int status;
	private String message;
	
	private ExceptionDetails(Builder builder) {
		this.title = builder.title;
		this.status = builder.status;
		this.message = builder.message;
	}

	public String getTitle() {
		return title;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public static final class Builder {
		private String title;
		private int status;
		private String message;
		
		private Builder() {
		}
		
		public static Builder newBuilder() {
			return new Builder();
		}
		
		public Builder title(String title) {
			this.title = title;
			return this;
		}
		
		public Builder status(int status) {
			this.status = status;
			return this;
		}
		
		public Builder message(String message) {
			this.message = message;
			return this;
		}
		
		public ExceptionDetails build() {
			ExceptionDetails details = new ExceptionDetails(this);
			return details;
		}
	}
}
