package com.conexa.desafio.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity(name = "pacientes")
public class Paciente extends Entidade {
    @Column(nullable = false)
	@NotBlank(message = "nome não pode estar em branco")
	private String nome;
	
	@Column(nullable = false, unique = true, length = 11)
	@NotBlank(message = "cpf não pode estar em branco")
	@Pattern(regexp = "[0-9]+", message = "O CPF deve conter apenas números")
	@Size(min = 11, max = 11, message = "O CPF deve conter 11 numeros")
	private String cpf;

	@OneToMany
	private List<Atendimento> atendimentos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
