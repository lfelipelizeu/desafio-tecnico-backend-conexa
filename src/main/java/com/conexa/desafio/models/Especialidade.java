package com.conexa.desafio.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

@Entity(name = "especialidades")
public class Especialidade extends Entidade {
    @Column(nullable = false, unique = true)
	@NotBlank(message = "nome não pode estar em branco")
	private String nome;

    @OneToMany
	private List<Medico> medicos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
