package com.conexa.desafio.models;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "token_blacklist")
public class TokenBlacklist extends Entidade {
    @Column(nullable = false)
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
