package com.conexa.desafio.utils;

import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.conexa.desafio.data.MedicoDetails;

public class JWTUtil {
	private static final int TOKEN_EXPIRACAO = 600000;
	private static final String TOKEN_SENHA = "590c1c9a-44d9-4c17-a2a1-f43ca2f07d08";

	public static String createToken(MedicoDetails medicoDetails) {
		String token = JWT.create()
				.withSubject(medicoDetails.getUsername())
				.withExpiresAt(new Date(System.currentTimeMillis() + TOKEN_EXPIRACAO))
				.sign(Algorithm.HMAC512(TOKEN_SENHA));
		
		return token;
	}
	
	public static String getRequestUsername(String token) {
		String username = JWT.require(Algorithm.HMAC512(TOKEN_SENHA))
				.build()
				.verify(token)
				.getSubject();
		
		return username;
	}
}
