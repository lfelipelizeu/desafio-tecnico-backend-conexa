package com.conexa.desafio.handlers;

import com.conexa.desafio.errors.BadRequestException;
import com.conexa.desafio.errors.ConflictException;
import com.conexa.desafio.errors.ExceptionDetails;
import com.conexa.desafio.errors.NotFoundException;
import com.conexa.desafio.errors.UnauthorizedException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(ConflictException.class)
	public ResponseEntity<?> handleConflictException(ConflictException exception) {
		ExceptionDetails details = ExceptionDetails.Builder
			.newBuilder()
			.title("Conflict")
			.status(HttpStatus.CONFLICT.value())
			.message(exception.getMessage())
			.build();
		
		return new ResponseEntity<>(details, HttpStatus.CONFLICT);
	}

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<?> handleBadRequestException(BadRequestException exception) {
		ExceptionDetails details = ExceptionDetails.Builder
			.newBuilder()
			.title("Bad Request")
			.status(HttpStatus.BAD_REQUEST.value())
			.message(exception.getMessage())
			.build();
		
		return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<?> handleNotFoundException(NotFoundException exception) {
		ExceptionDetails details = ExceptionDetails.Builder
			.newBuilder()
			.title("Not Found")
			.status(HttpStatus.NOT_FOUND.value())
			.message(exception.getMessage())
			.build();
		
		return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(UnauthorizedException.class)
	public ResponseEntity<?> handleUnathorizedException(UnauthorizedException exception) {
		ExceptionDetails details = ExceptionDetails.Builder
			.newBuilder()
			.title("Unauthorized")
			.status(HttpStatus.UNAUTHORIZED.value())
			.message(exception.getMessage())
			.build();
		
		return new ResponseEntity<>(details, HttpStatus.UNAUTHORIZED);
	}

    @Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionDetails details = ExceptionDetails.Builder
				.newBuilder()
				.title("Bad Request")
				.status(HttpStatus.BAD_REQUEST.value())
				.message(exception.getAllErrors().get(0).getDefaultMessage())
				.build();

		return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(
			HttpMessageNotReadableException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
				String message = "Você precisa enviar o body no formato JSON!";

				if (exception.getMostSpecificCause().getClass().getName().equals("java.time.format.DateTimeParseException"))
					message = "Você precisa passar uma data válida no formato AAAA-MM-DD";

				if (exception.getMostSpecificCause().getClass().getName().equals("com.fasterxml.jackson.databind.exc.InvalidFormatException"))
					message = "Você precisa passar uma data e horário no formato AAAA-MM-DDTHH:mm";

				ExceptionDetails details = ExceptionDetails.Builder
				.newBuilder()
				.title("Bad Request")
				.status(HttpStatus.BAD_REQUEST.value())
				.message(message)
				.build();

		return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
	}
}
