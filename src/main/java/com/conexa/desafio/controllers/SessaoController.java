package com.conexa.desafio.controllers;

import com.conexa.desafio.services.SessaoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/logoff")
public class SessaoController {
    @Autowired
    private SessaoService sessaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void logoff(@RequestHeader HttpHeaders header) {
        String token = header.get("Authorization").get(0).replace("Bearer ", "");
        sessaoService.logoff(token);
    }
}
