package com.conexa.desafio.controllers.protocols;

import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class NovoAtendimento {
	@NotNull(message = "dataHora não pode estar em branco")
	@Future(message = "A data do atendimento tem que ser no futuro!")
	private Date dataHora;

	@NotBlank(message = "cpfPaciente não pode estar em branco")
	private String cpfPaciente;
	
	public Date getDataHora() {
		return dataHora;
	}

	public String getCpfPaciente() {
		return cpfPaciente;
	}
}
