package com.conexa.desafio.controllers.protocols;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class NovoMedico {
	@NotBlank(message = "email não pode estar em branco")
	@Email(message = "Você inserir um email válido")
	private String email;

	@NotBlank(message = "senha não pode estar em branco")
	@Size(min = 5, max = 16, message = "A senha deve ter entre 5 e 16 caracteres")
	private String senha;

	@NotBlank(message = "confirmacaoSenha não pode estar em branco")
	private String confirmacaoSenha;

	@NotBlank(message = "cpf não pode estar em branco")
	@Pattern(regexp = "[0-9]+", message = "O CPF deve conter apenas números")
	@Size(min = 11, max = 11, message = "O CPF deve conter 11 numeros")
	private String cpf;

	@NotNull(message = "dataNascimento não pode estar em branco")
	@Past(message = "A data de nascimento deve estar no passado")
	private LocalDate dataNascimento;

	@NotBlank(message = "telefone não pode estar em branco")
	@Size(min = 10, max = 11, message = "O telefone deve ter 10 ou 11 números")
	@Pattern(regexp = "[0-9]+", message = "O telefone deve conter apenas números")
	private String telefone;

	@NotBlank(message = "especialidade não pode estar em branco")
	private String especialidade;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getConfirmacaoSenha() {
		return confirmacaoSenha;
	}
	public void setConfirmacaoSenha(String confirmacaoSenha) {
		this.confirmacaoSenha = confirmacaoSenha;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEspecialidade() {
		return especialidade;
	}
	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}
}
