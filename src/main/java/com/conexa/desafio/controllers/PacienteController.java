package com.conexa.desafio.controllers;

import java.util.List;

import javax.validation.Valid;

import com.conexa.desafio.errors.BadRequestException;
import com.conexa.desafio.errors.ConflictException;
import com.conexa.desafio.errors.UnauthorizedException;
import com.conexa.desafio.models.Paciente;
import com.conexa.desafio.services.PacienteService;
import com.conexa.desafio.services.SessaoService;
import com.conexa.desafio.utils.ValidaCpf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/pacientes")
public class PacienteController {
    @Autowired
    private PacienteService pacienteService;

    @Autowired
    private SessaoService sessaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Paciente registrarPaciente(@RequestBody @Valid Paciente paciente, @RequestHeader HttpHeaders header) {
        String token = header.get("Authorization").get(0).replace("Bearer ", "");
        Boolean tokenValido = sessaoService.verificarSeTokenValido(token);
        if (!tokenValido)
            throw new UnauthorizedException("Sessão inválida!");

        Boolean cpfValido = ValidaCpf.validarCpf(paciente.getCpf());
        if (!cpfValido)
            throw new BadRequestException("Cpf inválido");

        Boolean conflito = pacienteService.verificarSeJaExistePaciente(paciente.getCpf());
        if (conflito)
            throw new ConflictException("Este CPF já está cadastrado");

        return pacienteService.registrarPaciente(paciente);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Paciente> listarPacientes(@RequestHeader HttpHeaders header) {
        String token = header.get("Authorization").get(0).replace("Bearer ", "");
        Boolean tokenValido = sessaoService.verificarSeTokenValido(token);
        if (!tokenValido)
            throw new UnauthorizedException("Sessão inválida!");

        return pacienteService.listarPacientes();
    }
}
