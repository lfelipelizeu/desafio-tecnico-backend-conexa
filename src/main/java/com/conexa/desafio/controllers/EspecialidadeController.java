package com.conexa.desafio.controllers;

import java.util.List;

import javax.validation.Valid;

import com.conexa.desafio.errors.ConflictException;
import com.conexa.desafio.models.Especialidade;
import com.conexa.desafio.services.EspecialidadeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/especialidades")
public class EspecialidadeController {
    @Autowired
    private EspecialidadeService service;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Especialidade criarEspecialidade(@RequestBody @Valid Especialidade especialidade) {
        Boolean conflito = service.verificarSeJaExisteEspecialidade(especialidade.getNome());

        if (conflito)
            throw new ConflictException("Essa especialidade já está cadastrada no sistema!");

        return service.criarEspecialidade(especialidade);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Especialidade> listarEspecialidades() {
        return service.listarEspecialidades();
    }
}
