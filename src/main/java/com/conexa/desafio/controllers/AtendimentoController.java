package com.conexa.desafio.controllers;

import javax.validation.Valid;

import com.conexa.desafio.controllers.protocols.NovoAtendimento;
import com.conexa.desafio.errors.NotFoundException;
import com.conexa.desafio.errors.UnauthorizedException;
import com.conexa.desafio.models.Atendimento;
import com.conexa.desafio.models.Medico;
import com.conexa.desafio.models.Paciente;
import com.conexa.desafio.services.AtendimentoService;
import com.conexa.desafio.services.MedicoService;
import com.conexa.desafio.services.PacienteService;
import com.conexa.desafio.services.SessaoService;
import com.conexa.desafio.utils.JWTUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/attendance")
public class AtendimentoController {
    @Autowired
    private AtendimentoService atendimentoService;
    
    @Autowired
    private MedicoService medicoService;

    @Autowired
    private PacienteService pacienteService;

    @Autowired
    private SessaoService sessaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Atendimento novoAtendimento(@RequestBody @Valid NovoAtendimento novoAtendimento, @RequestHeader HttpHeaders header) {
        String token = header.get("Authorization").get(0).replace("Bearer ", "");
        Boolean tokenValido = sessaoService.verificarSeTokenValido(token);
        if (!tokenValido)
            throw new UnauthorizedException("Sessão inválida!");

        String cpfPaciente = novoAtendimento.getCpfPaciente();
        Paciente paciente = pacienteService.buscarPaciente(cpfPaciente);
        if (paciente == null)
            throw new NotFoundException("Paciente não encontrado");

        String emailMedico = JWTUtil.getRequestUsername(token);
        Medico medico = medicoService.buscarMedicoPeloEmail(emailMedico);

        return atendimentoService.criarAtendimento(novoAtendimento.getDataHora(), medico, paciente);
    }
}
