package com.conexa.desafio.controllers;

import javax.validation.Valid;

import com.conexa.desafio.controllers.protocols.NovoMedico;
import com.conexa.desafio.errors.BadRequestException;
import com.conexa.desafio.errors.ConflictException;
import com.conexa.desafio.errors.NotFoundException;
import com.conexa.desafio.models.Especialidade;
import com.conexa.desafio.models.Medico;
import com.conexa.desafio.services.EspecialidadeService;
import com.conexa.desafio.services.MedicoService;
import com.conexa.desafio.utils.ValidaCpf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/signup")
public class MedicoController {
    @Autowired
    private MedicoService medicoService;

    @Autowired
    private EspecialidadeService especialidadeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Medico cadastrarMedico(@RequestBody @Valid NovoMedico novoMedico) {
        Boolean senhasIguais = medicoService.verificarSeSenhasCoincidem(novoMedico.getSenha(), novoMedico.getConfirmacaoSenha());
        if (!senhasIguais)
            throw new BadRequestException("As senhas devem coincidir!");

        Boolean cpfValido = ValidaCpf.validarCpf(novoMedico.getCpf());
        if (!cpfValido)
            throw new BadRequestException("CPF inválido!");

        Medico emailCadastrado = medicoService.buscarMedicoPeloEmail(novoMedico.getEmail());
        if (emailCadastrado != null)
            throw new ConflictException("Email já cadastrado!");

        Medico cpfCadastrado = medicoService.buscarMedicoPeloCpf(novoMedico.getCpf());
        if (cpfCadastrado != null)
            throw new ConflictException("CPF já cadastrado!");

        Especialidade especialidade = especialidadeService.buscarEpecialidade(novoMedico.getEspecialidade());
        if (especialidade == null)
            throw new NotFoundException("Especialidade não encontrada");

        Medico medico = new Medico();
        medico.setEmail(novoMedico.getEmail());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        medico.setSenha(encoder.encode(novoMedico.getSenha()));
        medico.setCpf(novoMedico.getCpf());
        medico.setDataNascimento(novoMedico.getDataNascimento());
        medico.setTelefone(novoMedico.getTelefone());
        medico.setEspecialidade(especialidade);

        return medicoService.cadastrarMedico(medico);
    }
}
