package com.conexa.desafio.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.conexa.desafio.data.MedicoDetails;
import com.conexa.desafio.models.Medico;
import com.conexa.desafio.utils.JWTUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private AuthenticationManager authenticationManager;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
		try {
			Medico medico = new ObjectMapper().readValue(request.getInputStream(), Medico.class);
			
			return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(medico.getEmail(), medico.getSenha(), new ArrayList<>()));
		} catch (IOException exception) {
			throw new RuntimeException("Falha ao autenticar usuário", exception);
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
		MedicoDetails medicoDetails = (MedicoDetails) authResult.getPrincipal();
		
		String token = JWTUtil.createToken(medicoDetails);

		response.getWriter().write(token);
		response.getWriter().flush();
	}
}
