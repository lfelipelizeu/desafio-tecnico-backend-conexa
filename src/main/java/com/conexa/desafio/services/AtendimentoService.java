package com.conexa.desafio.services;

import java.util.Date;

import com.conexa.desafio.models.Atendimento;
import com.conexa.desafio.models.Medico;
import com.conexa.desafio.models.Paciente;
import com.conexa.desafio.repositories.AtendimentoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AtendimentoService {
    @Autowired
    private AtendimentoRepository atendimentoRepository;

    public Atendimento criarAtendimento(Date dataHora, Medico medico , Paciente paciente) {
        Atendimento novoAtendimento = new Atendimento();
        novoAtendimento.setDataHora(dataHora);
        novoAtendimento.setMedico(medico);
        novoAtendimento.setPaciente(paciente);

        return atendimentoRepository.save(novoAtendimento);
    }
}
