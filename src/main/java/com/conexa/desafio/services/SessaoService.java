package com.conexa.desafio.services;

import com.conexa.desafio.models.TokenBlacklist;
import com.conexa.desafio.repositories.TokenBlacklistRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SessaoService {
    @Autowired
    private TokenBlacklistRepository tokenBlacklistRepository;

    public void logoff(String token) {
        TokenBlacklist tokenBlacklist = new TokenBlacklist();
        tokenBlacklist.setToken(token);

        tokenBlacklistRepository.save(tokenBlacklist);
    }

    public Boolean verificarSeTokenValido(String token) {
        TokenBlacklist tokenBlacklist = tokenBlacklistRepository.findOneByToken(token);

        if (tokenBlacklist == null) {
            return true;
        } else {
            return false;
        }
    }
}
