package com.conexa.desafio.services;

import com.conexa.desafio.models.Medico;
import com.conexa.desafio.repositories.MedicoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MedicoService {
    @Autowired
    private MedicoRepository medicoRepository;

    public Boolean verificarSeSenhasCoincidem(String senha, String confirmacaoSenha) {
        if (senha.equals(confirmacaoSenha)) {
            return true;
        } else {
            return false;
        }
    }

    public Medico buscarMedicoPeloEmail(String email) {
        return medicoRepository.findOneByEmail(email);
    }

    public Medico buscarMedicoPeloCpf(String cpf) {
        return medicoRepository.findOneByCpf(cpf);
    }

    public Medico cadastrarMedico(Medico medico) {
        return medicoRepository.save(medico);
    }
}
