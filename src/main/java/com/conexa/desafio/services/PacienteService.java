package com.conexa.desafio.services;

import java.util.List;

import com.conexa.desafio.models.Paciente;
import com.conexa.desafio.repositories.PacienteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PacienteService {
    @Autowired
    private PacienteRepository pacienteRepository;

    public Boolean verificarSeJaExistePaciente(String cpf) {
        Paciente paciente = pacienteRepository.findOneByCpf(cpf);

        if (paciente == null)
            return false;

        return true;
    }

    public Paciente registrarPaciente(Paciente paciente) {
        return pacienteRepository.save(paciente);
    }

    public List<Paciente> listarPacientes() {
        return pacienteRepository.findAll();
    }

    public Paciente buscarPaciente(String cpf) {
        return pacienteRepository.findOneByCpf(cpf);
    }
}
