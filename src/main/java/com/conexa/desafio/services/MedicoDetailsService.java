package com.conexa.desafio.services;

import com.conexa.desafio.data.MedicoDetails;
import com.conexa.desafio.models.Medico;
import com.conexa.desafio.repositories.MedicoRepository;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class MedicoDetailsService implements UserDetailsService {
	
	private MedicoRepository repository;

	public MedicoDetailsService(MedicoRepository repository) {
		this.repository = repository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Medico medico = repository.findOneByEmail(username);
		
		if (medico == null)
			throw new UsernameNotFoundException("Email não encontrado");

		return new MedicoDetails(medico);
	}
}
