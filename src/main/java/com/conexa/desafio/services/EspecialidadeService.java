package com.conexa.desafio.services;

import java.util.List;

import com.conexa.desafio.models.Especialidade;
import com.conexa.desafio.repositories.EspecialidadeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EspecialidadeService {
    @Autowired
    private EspecialidadeRepository especialidadeRepository;

    public Boolean verificarSeJaExisteEspecialidade(String nome) {
        Especialidade especialidade = especialidadeRepository.findOneByNome(nome);

        if (especialidade == null)
            return false;

        return true;
    }

    public Especialidade buscarEpecialidade(String nome) {
        return especialidadeRepository.findOneByNome(nome);
    }

    public Especialidade criarEspecialidade(Especialidade especialidade) {
        return especialidadeRepository.save(especialidade);
    }

    public List<Especialidade> listarEspecialidades() {
        return especialidadeRepository.findAll();
    }
}
