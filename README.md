# Desafio Técnico Backend Conexa

A proposta do desafio é criar uma API REST que possbilita o login de médicos e o agendamento de atendimentos para pacientes.


## Conteúdo
- [Requisitos](#requisitos)
- [Stack utilizada](#stack-utilizada)
- [Criando o banco](#criando-o-banco)
- [Variáveis de Ambiente](#variáveis-de-ambiente)
- [Documentação da API](#documentação-da-api)
- [Rodando localmente](#rodando-localmente)
- [Rodando os testes](#rodando-os-testes)
- [Aprendizados](#aprendizados)

## Requisitos

- Java
- Java Development Kit (JDK)
- Maven
- MySQL


## Stack utilizada

- Java
- Spring
- MySQL


## Criando o banco

Este aplicativo precisa de um banco de dados MySQL para funcionar.\
Crie um banco e depois [configure as variáveis](#variáveis-de-ambiente), as tabelas serão criadas automaticamente.


## Variáveis de Ambiente

Para rodar esse projeto, você vai precisar editar as seguintes variáveis de ambiente no [application.properties](https://gitlab.com/lfelipelizeu/desafio-tecnico-backend-conexa/-/blob/master/src/main/resources/application.properties)

- `spring.datasource.url`: a URL para o [banco de dados](#criando-o-banco) utilizado no formato `jdbc:mysql://HOST:PORT/DBNAME?useSSL=false`, substituindo `HOST`, `PORT` e `DBNAME` pelo endereço do host do banco, porta do banco e nome do banco criado, respectivamente;

- `spring.datasource.username`: usuário do banco;

- `spring.datasource.password`: senha do usuário.


## Documentação da API
- [Criar uma especialidade](#criar-uma-especialidade)
- [Listar as especialidades cadastradas](#listar-as-especialidades-cadastradas)
- [Cadastrar um novo médico](#cadastrar-um-novo-médico)
- [Relizar login](#relizar-login)
- [Cadastrar um novo paciente](#cadastrar-um-novo-paciente)
- [Listar os pacientes cadastrados](#listar-os-pacientes-cadastrados)
- [Agendar um atendimento](#agendar-um-atendimento)
- [Realizar logoff](#realizar-logoff)

### Criar uma especialidade
- Retorna a especialidade criada

```http
POST /api/v1/especialidades
```
| Parâmetro     | Tipo       | Descrição                                                |
| :------------ | :--------- | :------------------------------------------------------- |
| `nome`        | `String`   | **Obrigatório**. O nome da especialidade.                |

### Listar as especialidades cadastradas
- Retorna uma lista de especialidades

```http
GET /api/v1/especialidades
```

### Cadastrar um novo médico
- Retorna o médico cadastrado

```http
POST /api/v1/signup
```

| Parâmetro   | Tipo       | Descrição                                   |
| :---------- | :--------- | :------------------------------------------ |
| `email`      | `String` | **Obrigatório**. O email do médico.  |
| `senha` | `String` | **Obrigatório**. A senha da novo cadastro. Deve ter entre 5 e 16 caracteres. |
| `confirmacaoSenha` | `String` | **Obrigatório**. Deve ser igual à `senha`. |
| `cpf` | `String` | **Obrigatório**. CPF do médico. Deve inserido apenas números. Um algoritimo de validação de CPF foi implementado, então utilize um CPF válido. |
| `dataNascimento` | `LocalDate` | **Obrigatório**. Data de nascimento do médico. Deve ser inserida no formato `AAAA-MM-DD`. |
| `telefone` | `String` | **Obrigatório**. Telefone do medico. Deve ser inserido apenas números. |
| `especialidade` | `String` | **Obrigatório**. Nome da especialidade do médico. |

### Relizar login

```http
POST /api/v1/login
```
- Retorna um token JWT

| Parâmetro   | Tipo       | Descrição                                                |
| :---------- | :--------- | :------------------------------------------------------- |
| `email`        | `String`  | **Obrigatório**. O email da conta cadastrada. |
| `senha`        | `String`  | **Obrigatório**. A senha da conta cadastrada. |

### Cadastrar um novo paciente

```http
POST /api/v1/pacientes
```
- Retorna o paciente cadastrado
- **Necessária autenticação**

| Parâmetro   | Tipo       | Descrição                                              |
| :---------- | :--------- | :----------------------------------------------------- |
| `nome`        | `String`  | **Obrigatório**. O nome do paciente. |
| `cpf` | `String` | **Obrigatório**. CPF do paciente. Deve inserido apenas números. Um algoritimo de validação de CPF foi implementado, então utilize um CPF válido. |

### Listar os pacientes cadastrados

```http
GET /api/v1/pacientes
```
- Retorna uma lista de pacientes
- **Necessária autenticação**

### Agendar um atendimento
- Retorna atendimento agendado
- **Necessária autenticação**

```http
POST /api/v1/attendance
```

| Parâmetro   | Tipo       | Descrição                                   |
| :---------- | :--------- | :------------------------------------------ |
| `dataHora`      | `Date` | **Obrigatório**. Data e horário do atendimento. Deve ser inserida no formato `AAAA-MM-DDTHH:mm` (não substitua o `T`). |
| `cpfPaciente` | `String` | **Obrigatório**. CPF do paciente atendido. |

### Realizar logoff

```http
POST /api/v1/logoff
```


## Rodando localmente

Clone o projeto

```bash
git clone https://gitlab.com/lfelipelizeu/desafio-tecnico-backend-conexa.git
```

Para rodar a aplicação, terá que rodar diretamente da IDE, porque eu não consegui deixar o JUnit como dependência, então o Maven retorna um erro se tentar rodar diretamente pelo terminal, a IDE (pelo menos a Spring Tool Suite 4) deixa você adiconar o JUnit diretamente. Então infelizmente para rodar a aplicação terá de ser diretamente pela IDE.


## Rodando os testes

Como explicado em [rodando localmente](#rodando-localmente), os testes também tem que ser rodados através da IDE.


## Aprendizados

Achei bem interessante o Spring, bem simples para criar os endpoints e bem fácil manipular banco também. Achei legal que várias configurações são simples para implementar, só colocar um decorator que o próprio framework já faz o resto, o lado ruim disso é que fica difícil de personalizar um pouco, por exemplo eu queria salvar os tokens no banco e depois excluí-los no logoff, mas não consegui, então tive que fazer a blacklist. Não consegui criar um middleware próprio, seria melhor ter criado um middleware para verificar se o token está na blacklist e permitir ou não, mas não consegui e coloquei o código de verificação manualmente nos endpoints que eu desejava essa verificação.

